const Discord = require('discord.js');
const client = new Discord.Client();
const auth = require('./auth.json');

let channel_to_data = {};
const PREFIX = "!";
client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

function getMembersToMention(msg, creator_to_room) {
    let membersMentioned = [];
    for (let i = 0; i < creator_to_room[msg.author.id].members.length; i++) {
        membersMentioned.push("<@" + creator_to_room[msg.author.id].members[i] + ">");
    }
    return membersMentioned;
}

client.on('message', msg => {
    if(msg.content.indexOf(PREFIX) != 0) return;

    const args = msg.content.slice(PREFIX.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();
    console.log(msg.channel.name);
    if (!(msg.channel.name in channel_to_data)) {
        channel_to_data[msg.channel.name] = {
            "name_to_room": {},
            "creator_to_room": {},
            "member_to_room": {}
        };
    }
    let name_to_room = channel_to_data[msg.channel.name]["name_to_room"];
    let creator_to_room = channel_to_data[msg.channel.name]["creator_to_room"];
    let member_to_room = channel_to_data[msg.channel.name]["member_to_room"];
    if (command == 'create') {
        let name = args[0];

        if (msg.author.id in creator_to_room) {
            msg.reply("Can't create more than room. Start your existing room with !start before you create another.");
        } else if (msg.author.id in member_to_room) {
            msg.reply("Can't create a room because you're already in room **" + member_to_room[msg.author.id].name + "**.");
        } else if (name != null) {
            if (name in name_to_room) {
                msg.reply("Room named **" + name + "** already exists. Pick another name.");
            } else {
                let room = {
                    creator: msg.author.id,
                    name: name,
                    members: [msg.author.id]
                };
                name_to_room[name] = room;
                creator_to_room[msg.author.id] = room;
                member_to_room[msg.author.id] = room;
                msg.reply("Room **" + name + "** created. 7/8 slots left.");
            }
        } else {
            msg.reply("Specify a name for your room with !create <room_name>");
        }
    } else if (command == 'join') {
        let name = args[0];
        if (msg.author.id in creator_to_room) {
            msg.reply("Can't join a room when you already created one. Start your existing room with !start before you create another.");
        } else if (msg.author.id in member_to_room) {
            msg.reply("You're already in room **" + member_to_room[msg.author.id].name + "**.");
        } else if (name == null) {
            let room_names = Object.keys(name_to_room);
            let fullest_nonfull_room = null;
            for (let i = 0; i < room_names.length; i++) {
                if (name_to_room[room_names[i]].members.length < 8) {
                    if (fullest_nonfull_room == null || name_to_room[room_names[i]].members.length > fullest_nonfull_room.members.length) {
                        fullest_nonfull_room = name_to_room[room_names[i]];
                    }
                }
            }
            if (fullest_nonfull_room != null) {
                fullest_nonfull_room.members.push(msg.author.id);
                member_to_room[msg.author.id] = fullest_nonfull_room;
                msg.reply("Joined room **" + fullest_nonfull_room.name + "**. " + (8 - fullest_nonfull_room.members.length) + "/8 slots left.");
            } else {
                msg.reply("No rooms open. Create one with !create <room_name>");
            }
        } else {
            if (name in name_to_room) {
                if (name_to_room[name].members.length >= 8) {
                    msg.reply("Room **" + name + "** is full.");
                } else if (name_to_room[name].members.includes(msg.author.id)) {
                    msg.reply("You're already in room **" + name + "**.");
                } else {
                    name_to_room[name].members.push(msg.author.id);
                    member_to_room[msg.author.id] = name_to_room[name];
                    msg.reply("Joined room **" + name + "**. " + (8 - name_to_room[name].members.length) + "/8 slots left.");
                }
            } else {
                msg.reply("Room **" + name + "** doesn't exist.");
            }
        }

    } else if (command == 'start') {
        if (msg.author.id in creator_to_room) {
            msg.reply("Starting room **" + creator_to_room[msg.author.id].name + "** with members " + getMembersToMention(msg, creator_to_room).join(', ') + ".");
            for (let i = 0; i < creator_to_room[msg.author.id].members.length; i++) {
                delete member_to_room[creator_to_room[msg.author.id].members[i]];
            }
            delete name_to_room[creator_to_room[msg.author.id].name];
            delete creator_to_room[msg.author.id];
        } else {
            msg.reply("You don't have a room to start.");
        }
    } else if (command == 'leave') {
        if (msg.author.id in creator_to_room) {
            let name = creator_to_room[msg.author.id].name;
            msg.reply("Deleting your room **" + name + "** which has members " + getMembersToMention(msg, creator_to_room) + ".");
            for (let i = 0; i < creator_to_room[msg.author.id].members.length; i++) {
                delete member_to_room[creator_to_room[msg.author.id].members[i]];
            }
            delete name_to_room[creator_to_room[msg.author.id].name];
            delete creator_to_room[msg.author.id];
        } else if (msg.author.id in member_to_room) {
            let room = member_to_room[msg.author.id];
            room.members = room.members.filter(member => member != msg.author.id);
            delete member_to_room[msg.author.id];
            msg.reply("Leaving room **" + room.name + "**. " + (8 - room.members.length) + "/8 slots left");
        } else {
            msg.reply("You aren't in a room.");
        }
    }


});

client.login(auth.token);
